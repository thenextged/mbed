/**
 * Driver for LCD devices based on the HD44780 controller
 * @author Gedeon Nyengele
 * @version 1.0
 * @date 5/7/2015
 */

/* Description 

LCD SIZES SUPPORTED:
1-line displays: 8x1, 16x1, 20x1, 24x1, 32x1, 40x1
2-line displays: 16x2, 20x2, 24x2, 32x2, 40x2
4-line displays: 16x4, 20x4, 40x4

!!! 4-bit mode ONLY ( No support for 8-bit mode )

SUPPORTED PLATFORM: mbed

DISCLAIMER: The author does not warrant the functions contained in the
            program will meet your requirements or that the operation 
            of the program will be uninterrupted or error-free.
            In no event will the author (Gedeon Nyengele) be liable
            for any damages, including any lost profit, lost savings, 
            lost patience or other incidental or consequential damage.
*/

#ifndef CRYST_LCD_H
#define CRYST_LCD_H

#include "mbed.h"

class Cryst_LCD : public Stream
{
public:
    // LCD Size
    enum LCDSize {
        LCD8x1, LCD16x1, LCD20x1, LCD24x1, LCD32x1, LCD40x1,  // 1-line displays
        LCD16x2, LCD20x2, LCD24x2, LCD32x2, LCD40x2,          // 2-line displays
        LCD16x4, LCD20x4, LCD40x4                             // 4-line displays
    };

    Cryst_LCD(PinName rs, PinName en, PinName db4, PinName db5,
        PinName db6, PinName db7, LCDSize size = LCD16x2);
    void cls();                           // Clear entire LCD screen
    void locate(int row, int col);        // Move cursor to speficic location
    void display_off();                   // Turn display off (data conserved)
    void display_on();                    // Turn display on
    void cursor_on();                     // Show cursor
    void cursor_off();                    // Hide cursor
    void cursor_blink();                  // Make cursor blink
    void cursor_no_blink();               // Make cursor no to blink
    void clear_line();                    // Clear content on current line and put cursor at the
                                          // beginning of the line
    int cols() { return getMaxCols(); }   // Get max number or columns for the LCD module used
    int rows() { return getMaxRows(); }   // Get max number or rows for the LCD module used
    int getCursorRow() { return _row; }   // Get row number where cursor is currently located
    int getCursorCol() { return _col; }   // Get column number where cursor is currently located

protected:
    virtual int _putc(int ch);
    virtual int _getc();    

private:
    void reset();
    void init();
    int getAddress(int row, int col);
    void writeData(int ch, bool use_default_timing = true);
    void writeCommand(int cmd, bool use_default_timing = true);
    void writeNibble(int nib);    
    void getMaxDimensions(int* rowCount, int* colCount);
    int getMaxRows();
    int getMaxCols();

private:
    DigitalOut _rs;
    DigitalOut _en;
    BusOut _data;
    int _row;
    int _col;
    LCDSize _size;
    int _displayStatus;
};



#endif // CRYST_LCD_H
