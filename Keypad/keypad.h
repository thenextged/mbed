/**
 * Driver for Keypad devices
 * @author Gedeon Nyengele
 * @version 1.0
 * @date 5/7/2015
 */

#ifndef KEYPAD_H
#define KEYPAD_H
#include "mbed.h"

template<unsigned char nRows, unsigned char nCols>
class Keypad
{
public:
    Keypad(char matrix[nRows][nCols], BusInOut* rows, BusInOut* cols);
    char getKeyChar();
    char getKeyChar(void (*wait_ms_fn)(int millis));
    ~Keypad(){};
    enum BTN_STATE { RELEASE, DEBOUNCE_PRESS, PRESSED, DEBOUNCE_RELEASE, DONE };

private:
    int getKey();

protected:
    char _matrix[nRows][nCols];
    BusInOut* _cols;
    BusInOut* _rows;
};

/** --------------------------------------------------------------------------
 * Constructor
 *----------------------------------------------------------------------------
 */
template<unsigned char nRows, unsigned char nCols>
Keypad<nRows, nCols>::Keypad(char matrix[nRows][nCols], BusInOut* rows, BusInOut* cols)
{

    // Copy keypad matrix
    for (int i = 0; i < nRows; i++) {
        for (int j = 0; j < nCols; j++) {
            _matrix[i][j] = matrix[i][j];
        }
    }

    // Create data bus for row pins
    _rows = rows;

    // Create data bus for column pins
    _cols = cols;
}

template<unsigned char nRows, unsigned char nCols>
char Keypad<nRows, nCols>::getKeyChar(void (*wait_ms_fn)(int millis)) {
    int key;
    int temp_key = 0;
    bool done = false;
    BTN_STATE state = RELEASE;

    while(!done) {
        switch(state) {
            case RELEASE:
                key = getKey();
                if (key < 0) {
                    state = RELEASE;
                } else {
                    state = DEBOUNCE_PRESS;
                    temp_key = key;
                }
                break;

            case DEBOUNCE_PRESS:
                if (wait_ms_fn == 0) wait_ms(30);
                else wait_ms_fn(30);
                key = getKey();
                if (key == temp_key) {
                    state = PRESSED;
                } else {
                    state = RELEASE;
                }
                break;

            case PRESSED:
                key = getKey();
                if (key == temp_key) {
                    state = PRESSED;
                } else {
                    state = DEBOUNCE_RELEASE;
                }
                break;

            case DEBOUNCE_RELEASE:
                if (wait_ms_fn == 0) wait_ms(30);
                else wait_ms_fn(30);
                key = getKey();
                if (key == temp_key) {
                    state = PRESSED;
                } else {
                    state = RELEASE;
                    done  = true;
                }
        }
    }

    return (char) temp_key;
}

/** --------------------------------------------------------------------------
 * Get Key Character
 *----------------------------------------------------------------------------
 */
template<unsigned char nRows, unsigned char nCols>
char Keypad<nRows, nCols>::getKeyChar()
{
    return getKeyChar(0);
}


// HELPER FUNCTIONS

template<unsigned char nRows, unsigned char nCols>
int Keypad<nRows, nCols>::getKey()
{
    int rowVal = (1 << nRows) - 1;
    int colVal = (1 << nCols) - 1;
    int row = -1, col = -1;
    unsigned int temp;

    // Read rows
    _cols->output();
    _cols->write(0);
    _rows->input();
    _rows->mode(PullUp);
    wait_us(5);
    rowVal = _rows->read();

    //Read colums
    _rows->output();
    _rows->write(0);
    _cols->input();
    _cols->mode(PullUp);
    wait_us(5);
    colVal = _cols->read();

    // Signal no key press
    if (rowVal == ((1 << nRows) - 1) || colVal == ((1 << nCols) - 1) )
        return -1;

    // Find row number
    temp = rowVal ^ ((1 << nRows) - 1);
    for (int i = 0; i < nRows; i++) {

        if ((temp & (1 << i)) > 0) {
            row = i;
            break;
        }
    }
    
    // Find column number
    temp = colVal ^ ((1 << nCols) - 1);
    for (int i = 0; i < nCols; i++) {
        if ((temp & (1 << i)) > 0) {
            col = i;
            break;
        }
    }

    if (row == -1 || col == -1)
        return -1;
    else {
        return _matrix[row][col];
    }
}

#endif // KEYPAD_H